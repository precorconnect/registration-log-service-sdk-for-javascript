import RegistrationLogSynopsisView from '../../src/registrationLogSynopsisView';
import dummy from '../dummy';

/*
 tests
 */
describe('RegistrationLogSynopsisView class', () => {
    describe('constructor', () => {
        it('throws if registrationId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        null,
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.facilityName,
                        dummy.extendedWarrantyStatus,
                        dummy.spiffStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'registrationId required');

        });
        it('sets registrationId', () => {
            /*
             arrange
             */
            const expectedRegistrationId = dummy.registrationId;

            /*
             act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    expectedRegistrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );

            /*
             assert
             */
            const actualRegistrationId =
                objectUnderTest.registrationId;

            expect(actualRegistrationId  ).toEqual(expectedRegistrationId );

        });
        it('throws if partnerSaleRegistrationId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        null,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.facilityName,
                        dummy.extendedWarrantyStatus,
                        dummy.spiffStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerSaleRegistrationId required');

        });
        it('sets partnerSaleRegistrationId', () => {
            /*
             arrange
             */
            const expectedPartnerSaleRegistrationId = dummy.partnerSaleRegistrationId;

            /*
             act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    expectedPartnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );
            /*
             assert
             */
            const actualPartnerSaleRegistrationId =
                objectUnderTest.partnerSaleRegistrationId;

            expect(actualPartnerSaleRegistrationId ).toEqual(expectedPartnerSaleRegistrationId);

        });
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        dummy.partnerSaleRegistrationId,
                        null,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.facilityName,
                        dummy.extendedWarrantyStatus,
                        dummy.spiffStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');

        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedPartnerAccountId = dummy.partnerAccountId;

            /*
             act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    expectedPartnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );

            /*
             assert
             */
            const actualPartnerAccountId =
                objectUnderTest.accountId;

            expect(actualPartnerAccountId).toEqual(expectedPartnerAccountId);

        });
        it('throws if sellDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        null,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.facilityName,
                        dummy.extendedWarrantyStatus,
                        dummy.spiffStatus
                    );


            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'sellDate required');
        });
        it('sets sellDate',() => {
            /**
             * arrange
             */
            const expectedSellDate = dummy.sellDate;

            /**
             * act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    expectedSellDate,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );


            /**
             assert
             */
            const actualSellDate
                = objectUnderTest.sellDate;

            expect(actualSellDate).toEqual(expectedSellDate);
        });
        it('does not throw if installDate is null',() => {
            /**
             arrange
             */
            const constructor = () =>
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    null,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );

            /**
             * act/assert
             */
                //expect(constructor).toThrowError(TypeError,'customerBrand required');
            expect(constructor).not.toThrow()
        });
        it('sets installDate',()=>{
            /**
             arrange
             */
            const expectedInstallDate = dummy.installDate;

            /**
             * act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    expectedInstallDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );

            /**
             * act
             */
            const actualInstallDate
                = objectUnderTest.installDate;

            expect(actualInstallDate).toEqual(expectedInstallDate);
        });
        it('throws if submittedDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        null,
                        dummy.facilityName,
                        dummy.extendedWarrantyStatus,
                        dummy.spiffStatus
                    );


            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'submittedDate required');
        });
        it('sets submittedDate',() => {
            /**
             * arrange
             */
            const expectedSubmittedDate = dummy.submittedDate;

            /**
             * act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    expectedSubmittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );
            /**
             assert
             */
            const actualSubmittedDate
                = objectUnderTest.submittedDate;

            expect(actualSubmittedDate).toEqual(expectedSubmittedDate);
        });
        it('throws if facilityName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        null,
                        dummy.extendedWarrantyStatus,
                        dummy.spiffStatus
                    );


            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'facilityName required');
        });
        it('sets facilityName',() => {
            /**
             * arrange
             */
            const expectedFacilityName= dummy.facilityName;

            /**
             * act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    expectedFacilityName,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    dummy.spiffStatus
                );
            /**
             assert
             */
            const actualFacilityName
                = objectUnderTest.facilityName;

            expect(actualFacilityName).toEqual(expectedFacilityName);
        });
        it('throws if extendedWarrantyStatus is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.facilityName,
                        null,
                        dummy.spiffStatus
                    );


            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'extendedWarrantyStatus required');
        });
        it('sets extendedWarrantyStatus',() => {
            /**
             * arrange
             */
            const expectedExtendedWarrantyStatus = dummy.extendedWarrantyStatus;

            /**
             * act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    expectedExtendedWarrantyStatus,
                    dummy.spiffStatus
                );
            /**
             assert
             */
            const actualExpectedExtendedWarrantyStatus
                = objectUnderTest.extendedWarrantyStatus;

            expect(actualExpectedExtendedWarrantyStatus).toEqual(expectedExtendedWarrantyStatus);
        });
        it('throws if spiffStatus is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new RegistrationLogSynopsisView(
                        dummy.registrationId,
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.facilityName,
                        dummy.extendedWarrantyStatus,
                        null
                    );


            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'spiffStatus required');
        });
        it('sets spiffStatus',() => {
            /**
             * arrange
             */
            const expectedSpiffStatus = dummy.spiffStatus;

            /**
             * act
             */
            const objectUnderTest =
                new RegistrationLogSynopsisView(
                    dummy.registrationId,
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.facilityName,
                    dummy.extendedWarrantyStatus,
                    expectedSpiffStatus
                );
            /**
             assert
             */
            const actualSpiffStatus
                = objectUnderTest.spiffStatus;

            expect(actualSpiffStatus).toEqual(expectedSpiffStatus);
        });
    });
});
