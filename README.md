## Description
Precor Connect registration log service SDK for javascript.


## Setup

**install via jspm**  
```shell
jspm install registration-log-service-sdk=bitbucket:precorconnect/registration-log-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import RegistrationLogServiceSdk,{RegistrationLogServiceSdkConfig} from 'registration-log-service-sdk'

const registrationLogServiceSdkConfig =
    new RegistrationLogServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const registrationLogServiceSdk =
    new RegistrationLogServiceSdk(
        registrationLogServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```