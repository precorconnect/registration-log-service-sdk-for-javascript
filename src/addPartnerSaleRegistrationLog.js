/**
 * @class {AddPartnerSaleRegistrationLog}
 */
export default class AddPartnerSaleRegistrationLog{

    _partnerSaleRegistrationId:number;

    _partnerAccountId:string;

    _accountName:string;

    _sellDate:string;

    _installDate:string;

    _submittedDate:string;

    _extendedWarrantyStatus:string;

    constructor(
        partnerSaleRegistrationId:number,
        partnerAccountId:string,
        accountName:string,
        sellDate:string,
        installDate:string,
        submittedDate:string,
        extendedWarrantyStatus:string
    ){

        if(!partnerSaleRegistrationId){
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        if(!accountName){
            throw new TypeError('accountName required');
        }
        this._accountName = accountName;

        if(!sellDate){
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

        this._installDate = installDate;

        if(!submittedDate){
            throw new TypeError('submittedDate required');
        }
        this._submittedDate = submittedDate;

        if(!extendedWarrantyStatus){
            throw new TypeError('extendedWarrantyStatus required');
        }
        this._extendedWarrantyStatus = extendedWarrantyStatus;

    }

    /**
     * getter methods
     */
    get partnerSaleRegistrationId():number{
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get partnerAccountId():string{
        return this._partnerAccountId;
    }

    /**
     * @returns {string}
     */
    get accountName():string {
        return this._accountName;
    }

    /**
     * @returns {string}
     */
    get sellDate():string{
        return this._sellDate;
    }

    /**
     * @returns {string}
     */
    get installDate():string{
        return this._installDate;
    }

    /**
     * @returns {string}
     */
    get submittedDate():string{
        return this._submittedDate;
    }

    /**
     * @returns {string}
     */
    get extendedWarrantyStatus():string{
        return this._extendedWarrantyStatus;
    }

    toJSON() {
        return {
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            partnerAccountId: this._partnerAccountId,
            accountName: this._accountName,
            sellDate: this._sellDate,
            installDate: this._installDate,
            submittedDate: this._submittedDate,
            extendedWarrantyStatus:this._extendedWarrantyStatus
        }
    }

}

