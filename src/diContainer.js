import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import RegistrationLogServiceSdkConfig from './registrationLogServiceSdkConfig';
import AddRegistrationLogFeature from './addRegistrationLogFeature';
import ListRegistrationLogFeature from './listRegistrationLogFeature';
import UpdateRegistrationLogFeature from './updateRegistrationLogFeature';
import ListDealerRepFirstAndLastNameFeature from './listDealerRepFirstAndLastNameFeature';


/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {RegistrationLogServiceSdkConfig} config
     */
    constructor(config:RegistrationLogServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(RegistrationLogServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {
        this._container.autoRegister(AddRegistrationLogFeature);
        this._container.autoRegister(ListRegistrationLogFeature);
        this._container.autoRegister(UpdateRegistrationLogFeature);
        this._container.autoRegister(ListDealerRepFirstAndLastNameFeature);
    }

}
