import {inject} from 'aurelia-dependency-injection';
import RegistrationLogServiceSdkConfig from './registrationLogServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import RegistrationDealerRepSynopsisView from './registrationDealerRepSynopsisView';

@inject(RegistrationLogServiceSdkConfig, HttpClient)
class ListDealerRepFirstAndLastNameFeature {

    _config:RegistrationLogServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:RegistrationLogServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists Registration Log by accountId
     * @param {number} registrationIds
     * @param {string} accessToken
     * @returns {Promise.<RegistrationLogSynopsisView>}
     */
    execute(registrationIds:number[],accessToken:string):Promise<RegistrationDealerRepSynopsisView> {

        return this._httpClient
            .createRequest(`registration-log/partnerRegistrationIds/${registrationIds}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response =>
                Array.from(
                    response.content
                )
            );
    }
}

export default ListDealerRepFirstAndLastNameFeature;
