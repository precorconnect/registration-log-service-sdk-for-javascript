import {inject} from 'aurelia-dependency-injection';
import RegistrationLogServiceSdkConfig from './registrationLogServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import UpdateRegistrationLog from './updateRegistrationLog';

@inject(RegistrationLogServiceSdkConfig, HttpClient)
class UpdateRegistrationLogFeature {

    _config:RegistrationLogServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:RegistrationLogServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * @param {UpdateRegistrationLog} request
     * @param {string} accessToken
     */
    execute(request:UpdateRegistrationLog,accessToken:string):Promise {

        return this._httpClient
            .createRequest(`registration-log/updateRegistrationLog`)
            .asPut()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()

    }
}

export default UpdateRegistrationLogFeature;
