import DiContainer from './diContainer';
import RegistrationLogServiceSdkConfig from './registrationLogServiceSdkConfig';
import RegistrationLogSynopsisView from './registrationLogSynopsisView';
import ListRegistrationLogFeature from './listRegistrationLogFeature';
import AddPartnerSaleRegistrationLog from './addPartnerSaleRegistrationLog';
import AddRegistrationLogFeature from './addRegistrationLogFeature';
import UpdateRegistrationLog from './updateRegistrationLog';
import UpdateRegistrationLogFeature from './updateRegistrationLogFeature';
import RegistrationDealerRepSynopsisView from './registrationDealerRepSynopsisView';
import ListDealerRepFirstAndLastNameFeature from './listDealerRepFirstAndLastNameFeature';


/**
 * @class {RegistrationLogServiceSdk}
 */
export default class RegistrationLogServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {RegistrationLogServiceSdkConfig} config
     */
    constructor(config:RegistrationLogServiceSdkConfig) {

        this._diContainer = new DiContainer(config);
    }

    /**
     * @param {AddPartnerSaleRegistrationLog} request
     * @param {string} accessToken
     * @returns {Promise.<number>}
     */
    addRegistrationLog(request:AddPartnerSaleRegistrationLog,
                            accessToken:string):Promise<number> {

        return this
            ._diContainer
            .get(AddRegistrationLogFeature)
            .execute(
                request,
                accessToken
            );
    }

    /**
     * @param {string} partnerAccountId
     * @param {string} accessToken
     * @returns {Promise.<RegistrationLogSynopsisView[]>}
     */
    listRegistrationLogWithId(partnerAccountId:string,
                                         accessToken:string):Promise<RegistrationLogSynopsisView[]> {

        return this
            ._diContainer
            .get(ListRegistrationLogFeature)
            .execute(
                partnerAccountId,
                accessToken
            );
    }

    /**
     * @param {UpdateRegistrationLog} request
     * @param {string} accessToken
     * @returns {void}
     */
    updateRegistrationLog(request:UpdateRegistrationLog,
                              accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdateRegistrationLogFeature)
            .execute(
                request,
                accessToken
            );
    }


    /**
     * @param {string} registrationIds
     * @param {string} accessToken
     * @returns {Promise.<RegistrationDealerRepSynopsisView[]>}
     */
    listDealerRepFirstAndLastName(registrationIds:number[],
                              accessToken:string):Promise<RegistrationDealerRepSynopsisView> {

        return this
            ._diContainer
            .get(ListDealerRepFirstAndLastNameFeature)
            .execute(
                registrationIds,
                accessToken
            );
    }
}
